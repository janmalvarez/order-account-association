# Order-Account associating script for null account_id mitigation

This is a simple script to call the production endpoint of core orders domain which lets us associate an accountId to an orderId. Required data is the orderId, the userId, and the account Id.




# Files

Please use "cruce.csv" as name for the CSV file that will be used as input for the script

It is important that the columns are only three, and they are named 

**account_id | user_id | order_id**

so it will look like this on the .csv file:

**account_id;user_id;order_id**

## Installation instructions

You should be able to install everything you need by just doing

> npm install

This script only requires nodejs to run, and the packages used are the node-fetch package, together with a csv-parser and a promise limiter (to avoid ECONNRESET errors when having too many concurrent requests)

## Running the script

After installing and placing the .csv file on the root path, you can just run


> npm run mitigate

And this will trigger the script.



(For reference about the endpoint used, please read https://falabella-gitlab.gitlab.io/catalyst/core/order/development/orders/swagger-ui/swagger_spec.html#/Order/updateOrderUsingPost)