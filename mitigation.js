var promiseLimit = require("promise-limit");
var limit = promiseLimit(5);

const fetch = require("node-fetch");
var domain = "https://www.falabella.services";
const csv = require("csv-parser");
const fs = require("fs");
const accountIdsArray = [];

fs.createReadStream("cruce.csv")
  .pipe(csv())
  .on("data", (data) => {
    const rowText= data["account_id;user_id;order_id"];
    if(rowText){
      accountIdsArray.push(rowText);
    }
  })
  .on("end", () => {
    const ordersSize = accountIdsArray.length;
    accountIdsArray.forEach(async (orderRow, index) => {
      //0:accountId, 1:userId, 2:orderId
      const [accountId, userId, orderId] = orderRow.split(";");
      console.log([accountId, userId, orderId]);

      //get account id from list of users based on userID
      //make POST request to /v1/orders/{orderId}/associateAccount
      const response = await limit(() => {
        return fetch(`${domain}/order/v1/orders/${orderId}/associateAccount`, {
          method: "POST",
          headers: {
            "content-type": "application/json",
            Authorization:
              "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5USTVOREE1TWpNMk1ETTNOVFkwUWtFek16ZzRSREJHT1RRNFFVVkJNRVl5UkVSQk9VWkJNdyJ9.eyJodHRwczovL29zbW9zLnNlcnZpY2VzIjp7InRlbmFudF9pZCI6ImY2YWRmNmE0LTdkMTMtNDQ4NS04N2RkLWY3OGQ1OTk3NTQzYyJ9LCJpc3MiOiJodHRwczovL29zbW9zLmF1dGgwLmNvbS8iLCJzdWIiOiJrb0wwVEhGZ05qU3A3czUwWThCc0M4QUgzRDRCRXY3U0BjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9mYWxhYmVsbGEuc2VydmljZXMiLCJpYXQiOjE2MTQzMTM4NjYsImV4cCI6MTYxNjkwNTg2NiwiYXpwIjoia29MMFRIRmdOalNwN3M1MFk4QnNDOEFIM0Q0QkV2N1MiLCJndHkiOiJjbGllbnQtY3JlZGVudGlhbHMifQ.lZSrWCHbYFMyMkvyDMYYzJZ3b9hAk9lOSgD5Bu_UJ1lL3ib25_VFcv1WOg0E5qNa82kKpAtJtCIZCflyXq0G9nu4kSv8xc9BaXUkgnIx9krPCusWeW9HEuXyentg67g4uGRxQFkoegD4rW_Ajk3p7nzAOvupFkSctEPvkVE7V--LpIXOEMPIQJMPBtnV3Rn-gHSuBPxUvGhgo8H5WANqmc2FAulRwVHFeVI4eTjV5zXwHMDzK9Y-53gIru9jWggYMsVxcO_zr2G9H5cb1A2haZJZGHz767oNMYNOn28Vjqfobb-U7e_BLoXALcG32A2uKH31ZZhlyTaaiu0fiqLxrg",
          },
          body: JSON.stringify({
            data: {
              order: {
                userId: userId,
                accountId: accountId,
              },
            },
          }),
        });
      });
      const responseText= await response.json();
      console.log(responseText, "ORDER NUMBER "+(index+1)+ " out of "+ ordersSize)
    });
  });